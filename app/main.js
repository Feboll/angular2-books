"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var book_list_module_1 = require('./book-list/book-list.module');
var platform = platform_browser_dynamic_1.platformBrowserDynamic();
platform.bootstrapModule(book_list_module_1.BookListModule);
//# sourceMappingURL=main.js.map