import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BookListModule } from './book-list/book-list.module';
const platform = platformBrowserDynamic();
platform.bootstrapModule(BookListModule);
