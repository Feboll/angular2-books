import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BookListComponent }   from './book-list.component';
@NgModule({
    imports: [BrowserModule],
    declarations: [BookListComponent],
    bootstrap: [BookListComponent]
})
export class BookListModule { }
